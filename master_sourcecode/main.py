import os.path
import csv
import numpy as np
import pandas as pd
import json
import math
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.optimizers import Adam
from keras.losses import MeanSquaredError
from sklearn.linear_model import LinearRegression

jsondata = open('C:/Users/fmbou/OneDrive/Desktop/university/Master/Master Thesis/actualData/csvjsonLabel.json')
jsondata = json.load(jsondata)
"We build dataset based on the data extracted from CodeMR"
def codemr_data():
    reader = csv.reader(open('dataset.csv', 'r'))
    d = {}
    k = 0
    keys = []
    rawData = [row for row in reader]
    n = len(rawData) -1
    data = []
    maxs = {"wmc":float("-inf"),
            "loc":float("-inf"),
            "cmloc":float("-inf"),
            "nof":float("-inf"),
            "nosf":float("-inf"),
            "nosm":float("-inf")}
    for row in rawData:
        values = row[0].split(";")[1:]
        gId = row[0].split(";")[0].replace("_","")[-2:]

        if k == 0:
            keys = values
        else:
            item = {}
            for i in range(len(keys)):
                key = keys[i].lower()
                item['branch'] = gId
                item[key] = float(values[i])
                if key in maxs:
                    if item[key]>maxs[key]:
                        maxs[key] = item[key]
            data.append(item)
        k = k + 1
    #print(data)
    #print(maxs)
    diffData = []
    for i in range(len(data)-1):
        diffData.append([])
        for j in range(i+1,len(data)):
            diffDIT = min(data[i]["dit"],data[j]["dit"])
            diffWMC = abs(data[i]["wmc"]-data[j]["wmc"])/maxs["wmc"]
            diffLOC = abs(data[i]["loc"]-data[j]["loc"])/maxs["loc"]
            diffCMLOC = abs(data[i]["cmloc"]-data[j]["cmloc"])/maxs["cmloc"]
            diffNOF = abs(data[i]["nof"]-data[j]["nof"])/maxs["nof"]
            diffNOSF = abs(data[i]["nosf"]-data[j]["nosf"])/maxs["nosf"]
            diffNOSM = abs(data[i]["nosm"]-data[j]["nosm"])/maxs["nosm"]
            diffNORM = max(data[i]["norm"],data[j]["norm"])
            diffData[i].append({
                "group": data[i]["branch"]+"-"+data[j]["branch"],
                "dit": diffDIT,
                "wmc": diffWMC,
                "loc": diffLOC,
                "cmloc": diffCMLOC,
                "nof": diffNOF,
                "nosf": diffNOSF,
                "nosm": diffNOSM,
                "norm": diffNORM
            })
    dataset = [item for l in diffData for item in l]
    #print(sum(len(row) for row in diffData))
    # Use a breakpoint in the code line below to debug your script.
    #opening json file with results from git diff and add its data into group

    for group in dataset:
        for groupgit in jsondata:
            if group["group"]==groupgit["Groups"]:
                group["files_changed"] = groupgit["Files changed"]
                group["insertions"] = groupgit["Insertions"]
                group["deletions"] = groupgit["Deletions"]
                group["label"] = groupgit["label"]
                group["input"] = groupgit["Input"]
                break

    #print([i for i in jsondata if i["Groups"]in ["4D-6D","1C-7C","1C-6D"]])
    print(dataset)
    return dataset

dataset = codemr_data()
print(dataset)
# The next step is now to implement a deep learning which takes a pair of branches as input and predicts the output label. This program will be trained and evaluated.
X = []
Y = []
for eachdata in dataset:
    X.append(eachdata)
    #X.append(eachdata['input', 'file_changed', 'insertions', 'deletions', 'dit', 'loc', 'wmc', 'cmloc', 'nof', 'nosf', 'nosm', 'norm'])
    Y.append(eachdata['label'])
df = pd.DataFrame(X)
df2 = pd.DataFrame(Y)
X = df[['dit', 'wmc','loc','cmloc','nof','nosf','nosm','norm','insertions','files_changed','deletions',]]
Y= df2
print("fine till here")
scaler = StandardScaler()
print(X.shape)
print(Y.shape)
# We start working on the machine learning model
X_train, X_test, Y_train, Y_test = train_test_split(X, Y,test_size=0.33, random_state=1)
X_train = scaler.fit_transform(X_train)
Y_train = scaler.fit_transform(Y_train)
model = Sequential([BatchNormalization(),
                    Dense(10, input_dim=2),
                    Dense(1,)
                     ])
opt = Adam(learning_rate=0.3)
model.compile(optimizer=opt, loss=MeanSquaredError(), metrics=['mean_squared_error'])
hist = model.fit(X, Y, batch_size=5, epochs=100, validation_data=(X_test, Y_test))